﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MasterMind.Api.Models
{
    public class ChatModel
    {
        public string NickName { get; set; }
        public string Message { get; set; }
    }
}
