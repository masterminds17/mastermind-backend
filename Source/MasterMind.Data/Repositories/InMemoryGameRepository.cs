﻿using System;
using System.Collections.Concurrent;
using MasterMind.Data.DomainClasses;

namespace MasterMind.Data.Repositories
{
    public class InMemoryGameRepository : IGameRepository
    {
        private ConcurrentDictionary<Guid,IGame > _games;
        public InMemoryGameRepository()
        {
            _games = new ConcurrentDictionary<Guid, IGame>();
        }

        public IGame Add(IGame newGame)
        {
            Guid id = Guid.NewGuid();
            _games.TryAdd(id, newGame);
            _games[id].Id = id;
            return _games[id];
            
        }

        public IGame GetById(Guid id)
        {
            for (int i = 0; i < _games.Count; i++)
            {
                if (!_games.ContainsKey(id))
                {
                    throw new DataNotFoundException();
                }
            }
            return _games[id];
            
        }

        public void DeleteById(Guid id)
        {
            if (_games.ContainsKey(id))
            {
                if(_games.ContainsKey(id))
                {
                    IGame gameroom = _games[id];
                    _games.TryRemove(id, out gameroom);
                }
              
            }
        }
    }
}