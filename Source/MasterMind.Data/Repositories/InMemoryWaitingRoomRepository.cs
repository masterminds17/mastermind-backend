﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using MasterMind.Data.DomainClasses;

namespace MasterMind.Data.Repositories
{
    public class InMemoryWaitingRoomRepository : IWaitingRoomRepository
    {
        private ConcurrentDictionary<Guid, WaitingRoom> waitingRooms;

        public InMemoryWaitingRoomRepository()
        {
            waitingRooms = new ConcurrentDictionary<Guid, WaitingRoom>();
        }

        public WaitingRoom Add(WaitingRoom newWaitingRoom)
        {
            Guid id = Guid.NewGuid();
            waitingRooms.TryAdd<Guid, WaitingRoom>(id, newWaitingRoom);
            waitingRooms[id].Id = id;
            return waitingRooms[id];
        }

        public WaitingRoom GetById(Guid id)
        {
            if (waitingRooms.ContainsKey(id))
            {
                return waitingRooms[id];
            }
            else
            {
                throw new DataNotFoundException();
            }
        }

        public ICollection<WaitingRoom> GetAll()
        {
            return waitingRooms.Values;
        }

        public void DeleteById(Guid id)
        {
            if (waitingRooms.ContainsKey(id))
            {
                WaitingRoom waitingroom = waitingRooms[id];
                waitingRooms.TryRemove(id, out waitingroom);
            }  
        }
    }
}