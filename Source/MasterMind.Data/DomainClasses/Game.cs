﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace MasterMind.Data.DomainClasses
{
    public class Game : IGame
    {
        private int _currentRound;
        public Guid Id { get; set; }
        public GameSettings Settings { get; set; }
        public IList<IPlayer> Players { get; }
        public string[] PossibleColors { get;}
        public int CurrentRound => _currentRound;
        private IPlayer winner = null;
        private string[] _codeToGuess;
        private ConcurrentDictionary<Guid, int> amountGuessesPerPlayer = new ConcurrentDictionary<Guid, int>();
        
        /// <summary>
        /// Constructs a Game object and generates a code to guess.
        /// </summary>
        public Game(GameSettings settings, IList<IPlayer> players)
        {
            string[] allColors = {"blue", "red", "yellow", "purple", "green", "brown", "orange", "pink"};
            _currentRound = 1;
            Random number = new Random();
            Id = new Guid();
            Players = players;
            foreach (IPlayer user in players)
            {
                amountGuessesPerPlayer.TryAdd(user.Id, 0);
            }
            Settings = settings;
            PossibleColors = new string[settings.AmountOfColors];
            for(int i = 0; i < PossibleColors.Length; i++)
            {
                PossibleColors[i] = allColors[i];
            }
            _codeToGuess = new string[settings.CodeLength];

            for (int i = 0; i < _codeToGuess.Length; i++)
            {
                int index = number.Next(0, PossibleColors.Length);
                Find:
                foreach (string color in _codeToGuess)
                {
                    if (color == PossibleColors[index])
                    {
                        index = number.Next(0, PossibleColors.Length);
                        goto Find;
                    }
                }

                _codeToGuess[i] = PossibleColors[index];
            }

            
        }

        public CanGuessResult CanGuessCode(IPlayer player, int roundNumber)
        {
            if (amountGuessesPerPlayer.TryGetValue(player.Id, out int guesses) == true)
            {
                if (guesses == Settings.MaximumAmountOfGuesses)
                {
                    return CanGuessResult.MaximumReached;
                }
            }
           
            foreach (IPlayer user in Players)
            {
                if (amountGuessesPerPlayer[user.Id] < amountGuessesPerPlayer[player.Id])
                {
                    return CanGuessResult.MustWaitOnOthers;
                }
            }

            if (CurrentRound > roundNumber)
            {
                return CanGuessResult.NotAllowedDueToGameStatus;
            }
            else if (CurrentRound < roundNumber)
            {
                return CanGuessResult.RoundNotStarted;
            }
            else
            {
                return CanGuessResult.Ok;

            }
        }

        public GuessResult GuessCode(string[] colors, IPlayer player)
        {
            GuessResult guess = new GuessResult(colors);
            guess.Verify(_codeToGuess);
            if (guess.CorrectColorAndPositionAmount == _codeToGuess.Length) 
            {
                _currentRound += 1;
                winner = player;
            }
            else
            {
                amountGuessesPerPlayer[player.Id] += 1;
            }

            bool everyoneMaxGuesses = true;
            foreach (IPlayer user in Players)
            {
                if (amountGuessesPerPlayer[user.Id] != Settings.MaximumAmountOfGuesses)
                {
                    everyoneMaxGuesses = false;
                }
            }

            if (everyoneMaxGuesses == true)
            {
                _currentRound++;
                foreach (IPlayer user in Players)
                {
                    amountGuessesPerPlayer[user.Id] = 0;
                }
            }
            return guess;
        }

        public GameStatus GetStatus()
        {
            GameStatus status = new GameStatus();
            if (winner != null || CurrentRound > Settings.AmountOfRounds)
            {
                status.GameOver = true;
                status.FinalWinner = winner;
                status.CurrentRoundNumber += 1;
            }
            else
            {
                status.CurrentRoundNumber = CurrentRound;
                status.GameOver = false;
                status.FinalWinner = winner;
            }

            return status;
        }
    }
}