﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MasterMind.Data.DomainClasses
{
    public class Chat
    {
        public string NickName { get; set; }
        public string Message { get; set; }
    }
}
