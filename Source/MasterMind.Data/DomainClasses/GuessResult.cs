﻿using System.Collections.Generic;
using System.Linq;

namespace MasterMind.Data.DomainClasses
{
    public class GuessResult
    {
        private int _correctColorAndPositionAmount;
        private int _correctColorAmount;

        public string[] Colors { get; set; }

        public virtual int CorrectColorAndPositionAmount => _correctColorAndPositionAmount; //must be virtual for some automated test to work!

        public virtual int CorrectColorAmount => _correctColorAmount; //must be virtual for some automated test to work!

        public GuessResult(string[] colors)
        {
            Colors = new string[colors.Length];
            Colors = colors;
            _correctColorAmount = 0;
            _correctColorAndPositionAmount = 0;
        }

        public void Verify(string[] codeToGuess)
        {
            List<string> duplicates = new List<string>();

            for (int i = 0; i < codeToGuess.Length; i++)
            {
                if (!duplicates.Contains(Colors[i]))
                {
                    duplicates.Add(Colors[i]);
                }
                else
                {
                    duplicates.Add("");
                }
            }
 
            for (int i = 0; i < codeToGuess.Length; i++)
            {
                if (Colors[i] == codeToGuess[i])
                {
                    _correctColorAndPositionAmount++;
                }
                else if (codeToGuess.Contains(duplicates[i]))
                {
                    _correctColorAmount++;
                }
            }
        }
    }
}