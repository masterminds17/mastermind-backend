﻿using System;
using MasterMind.Business.Models;
using MasterMind.Data.DomainClasses;
using MasterMind.Data.Repositories;
using System.Collections.Generic;
using System.Linq;
using MasterMind.Data;
using System.Collections.Concurrent;

namespace MasterMind.Business.Services
{
    public class WaitingRoomService : IWaitingRoomService
    {
        private IWaitingRoomRepository _waitingRoomRepository;

        public WaitingRoomService(IWaitingRoomRepository waitingRoomRepository)
        {
            _waitingRoomRepository = waitingRoomRepository;
        }

        public ICollection<WaitingRoom> GetAllAvailableRooms()
        {
            ICollection<WaitingRoom> availableWaitingRooms = new List<WaitingRoom>();
            ICollection<WaitingRoom> allRooms = _waitingRoomRepository.GetAll();

            foreach (WaitingRoom waiting in allRooms)
            {
                if (waiting.Users.Count < 4)
                {
                    availableWaitingRooms.Add(waiting);
                }
            }

            return availableWaitingRooms;
        }

        public WaitingRoom CreateRoom(WaitingRoomCreationModel roomToCreate, User creator)
        {
            WaitingRoom waitingRoom = new WaitingRoom(roomToCreate.Name, creator, roomToCreate.GameSettings);
            ICollection<WaitingRoom> waitingRooms = _waitingRoomRepository.GetAll();
            bool alreadyExists = false;

            foreach (WaitingRoom waiting in waitingRooms)
            {
                if (waiting.Name == waitingRoom.Name)
                {
                    alreadyExists = true;
                }
            }

            if (alreadyExists == false)
            {
                waitingRoom = _waitingRoomRepository.Add(waitingRoom);
            }
            else
            {
                throw new ApplicationException();
            }

            return waitingRoom;
        }

        public WaitingRoom GetRoomById(Guid id)
        {
            return _waitingRoomRepository.GetById(id);
        }

        public bool TryJoinRoom(Guid roomId, User user, out string failureReason)
        {
            string reason = "";
            try
            {
                WaitingRoom waitingRoom = _waitingRoomRepository.GetById(roomId);
                
                if (waitingRoom.Users.Count == 4)
                {
                    failureReason = "Room is already full";
                    return false;
                }
                else
                {
                    foreach (User person in waitingRoom.Users)
                    {
                        if (person.Id == user.Id)
                        {
                            failureReason = "The player is already in the room";
                            return false;
                        }
                    }
                }
                IList<User> users = waitingRoom.Users;
                users.Add(user);
                waitingRoom.Users = users;
                
                failureReason = reason;
                return true;
            } catch (Exception)
            {
                failureReason = "Room doesn't exist";
                return false;
            }
        }

        public bool TryLeaveRoom(Guid roomId, User user, out string failureReason)
        {
            try
            {
                WaitingRoom waitingRoom = _waitingRoomRepository.GetById(roomId);

                if (waitingRoom.CreatorUserId == user.Id)
                {
                    _waitingRoomRepository.DeleteById(roomId);
                    failureReason = "";
                    return true;
                }

                for (int i = 0; i < waitingRoom.Users.Count; i++)
                {
                    if (waitingRoom.Users[i].Id == user.Id)
                    {
                        IList<User> users = waitingRoom.Users;
                        users.RemoveAt(i);
                        waitingRoom.Users = users;
                        failureReason = "";
                        return true;
                    }
                }

                failureReason = "This person is not linked to a room";
                return false;
            } catch (Exception)
            {
                failureReason = "The room does not exist";
                return false;
            }
        }

        public List<Chat> SendChat(Guid roomId, string nickname, string message)
        {
            WaitingRoom waitingRoom = _waitingRoomRepository.GetById(roomId);
            List<Chat> roomchats = waitingRoom.Chats;
            Chat chat = new Chat();
            chat.NickName = nickname;
            chat.Message = message;
            roomchats.Add(chat);
            waitingRoom.Chats = roomchats;

            return roomchats;
        }
    }
}