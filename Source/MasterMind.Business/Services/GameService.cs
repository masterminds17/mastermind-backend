﻿using MasterMind.Data.DomainClasses;
using System;
using System.Linq;
using MasterMind.Data.Repositories;
using System.Collections;
using System.Collections.Generic;

namespace MasterMind.Business.Services
{
    public class GameService : IGameService
    {
        private IGameRepository _gameRepository;
        private IWaitingRoomService _waitingRoomService;

        public GameService(IWaitingRoomService waitingRoomService, IGameRepository gameRepository)
        {
            _gameRepository = gameRepository;
            _waitingRoomService = waitingRoomService;
        }

        public IGame StartGameForRoom(Guid roomId)
        {
            IList<IPlayer> players = new List<IPlayer>();
            if (_waitingRoomService.GetRoomById(roomId).Users.Count < 2)
            {
                throw new ApplicationException();
            }
            foreach (User user in _waitingRoomService.GetRoomById(roomId).Users)
            {
                IPlayer player = user;
                players.Add(player);
            }
            Game game = new Game(_waitingRoomService.GetRoomById(roomId).GameSettings, players);
            _gameRepository.Add(game);
            _waitingRoomService.GetRoomById(roomId).GameId = game.Id;
            return game;
        }

        public IGame GetGameById(Guid id)
        {
            return _gameRepository.GetById(id);
        }

        public CanGuessResult CanGuessCode(Guid gameId, IPlayer player, int roundNumber)
        {
            return _gameRepository.GetById(gameId).CanGuessCode(player, roundNumber);
        }

        //ok
        public GuessResult GuessCode(Guid gameId, string[] colors, IPlayer player)
        {
            IGame game = _gameRepository.GetById(gameId);

            if (game.CanGuessCode(player, game.CurrentRound) == CanGuessResult.Ok)
            {
                return game.GuessCode(colors, player);
            }
            else
            {
                throw new ApplicationException();
            }
        }

        public GameStatus GetGameStatus(Guid gameId)
        {
            return _gameRepository.GetById(gameId).GetStatus();
        }
    }
}